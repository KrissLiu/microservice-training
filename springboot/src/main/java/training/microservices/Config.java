package training.microservices;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

@Service
@RefreshScope
public class Config {

    @Value(value="${message}")
    String message;

    public String getMessage() {
        return message;
    }
}
