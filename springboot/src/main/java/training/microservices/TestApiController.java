package training.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping(value= "/test")
public class TestApiController {

    @Autowired
    Config config;

    @RequestMapping(value = "/hello/{word}", method = RequestMethod.GET)
    String hello(@PathVariable(value = "word") String word) {
        return config.getMessage() + word;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    Integer add(@RequestParam(name = "x") Integer x, @RequestParam(name = "y") Integer y) {
        return x + y;
    }
}