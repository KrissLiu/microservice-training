package training.microservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import training.microservices.client.TestApi;

@RestController()
@RequestMapping(value = "/client")
public class ClientApiController {

    @Autowired
    TestApi testApi;

    @RequestMapping(value = "/double/{number}", method = RequestMethod.GET)
    public Integer doubleNumber(@PathVariable(value = "number") Integer number) {
        return testApi.add(number, number);
    }

    @RequestMapping(value = "/talk/{word}", method = RequestMethod.GET)
    public String talk(@PathVariable(value = "word") String word) {
        return "TALK: " + testApi.hello( word);
    }
}