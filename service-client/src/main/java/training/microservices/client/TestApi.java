package training.microservices.client;


import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping(value = "/test")
@FeignClient(name = "training.microservice.testapi")
public interface TestApi {

    @RequestMapping(value = "/hello/{word}", method = RequestMethod.GET)
    String hello(@PathVariable(value = "word") String word);

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    Integer add(@RequestParam(name = "x") Integer x, @RequestParam(name = "y") Integer y);
}